#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

void getIdFromUser(string&);
int openFile(fstream&);
void searchFile(fstream&, string);
bool shouldWeAppendToFile();
void appendToFile(fstream&, string);

int main()
{
	fstream roster_file;
	string user_id;

	int result = openFile(roster_file);

	if (result == -1)
	{
		cout << "Error reading file, exiting" << endl;
		return result;
	}

	while (1 == 1)
	{
		getIdFromUser(user_id);

		if (user_id.compare("0") == 0)
		{
			return 0;
		}

		searchFile(roster_file, user_id);
	}
}

void getIdFromUser(string &user_id)
{
	cout << "Please enter a user id (Enter 0 to quit): ";
	cin >> user_id;
}

int openFile(fstream &file)
{
	string path = "roster.txt";
	file.open(path.c_str(), fstream::in | fstream::out | fstream::app);

	return file ? 0 : -1;
}

void searchFile(fstream& file, string user_id)
{
	vector<string> lines;
	string id, line, name;
	bool found = 0;

	file.clear();
	file.seekg(0);

	while(file >> id >> name)
	{
		if (id.compare(user_id) == 0)
		{
			found = 1;
			break;
		}
	}

	if (found == 1)
	{
		cout << "Found " << id << ", their name is: " << name << endl;
	}
	else
	{
		cout << "No such person with id " << user_id << endl;

		if (shouldWeAppendToFile())
		{
			// "Bad" bit may be set, reset all flags
			file.clear();
			appendToFile(file, user_id);
		}
	}
}

bool shouldWeAppendToFile()
{
	string answer;
	cout << "Would you like to add a user (y/n): ";
	cin >> answer;

	return answer.compare("y") == 0 || answer.compare("Y") == 0;
}

void appendToFile(fstream& file, string user_id)
{
	string name;
	cout << "Enter the username for user id " << user_id << ": ";
	cin >> name;
	file << user_id << '\t' << name << endl;
}
